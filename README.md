# How to prepare this repo?

Here:
1) create a directory tree with the name of your project (e.g., `./net/socketproject/`)
2) create Makefile inside this directory to specify how to obtain and build your project


# After that, in Turris-build/build:
1) add this into feeds.conf
`src-git myfeed https://gitlab.fit.cvut.cz/cejkato2/myopenwrtfeed;master`
2) `./scripts/feeds update myfeed`
3) `./scripts/feeds install -a`
4) `make defconfig`
    or `make menuconfig`
5) `make package/socketproject/compile`
6) you can probably find your package in bin/packages/

# Warning:

- Your Makefile really needs to be inside a subdirectory; it does not work to have Makefile in root directory or inside just one-level directory!


